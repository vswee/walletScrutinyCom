---
wsId: beFiWeb3
title: BeFi - Web3.0 Wallet
altTitle: 
authors:
- danny
users: 10000
appId: com.cube.beepay
appCountry: 
released: Jul 14, 2021
updated: 2023-02-04
version: 2.1.3
stars: 4.6
ratings: 
reviews: 2
size: 
website: https://befiwallet.io/
repository: 
issue: 
icon: com.cube.beepay.png
bugbounty: 
meta: ok
verdict: nobtc
date: 2023-03-31
signer: 
reviewArchive: 
twitter: BeFiWalletverse
social:
- https://t.me/befiwallet
redirect_from: 
features: 

---

This wallet only supports altchains like ETH, TRON, KLAY and others. Thus, it only supports tokenized "BTC" such wBTC, which is not really Bitcoin.
