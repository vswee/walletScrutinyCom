---
wsId: 
title: Forex Broker
altTitle: 
authors:
- danny
users: 10000
appId: com.bucdev.bestforexbroker
appCountry: 
released: 2018-04-17
updated: 2020-10-09
version: 1.1.3
stars: 
ratings: 
reviews: 
size: 
website: https://www.knightgames.co
repository: 
issue: 
icon: com.bucdev.bestforexbroker.png
bugbounty: 
meta: obsolete
verdict: wip
date: 2023-03-30
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 
features: 

---

We tried installing this but the app said it was made for an older version of Android. 

The homepage of the site also points to knightgames.co which does not have a register or sign-up/sign-in option. It only has a contact form. 
