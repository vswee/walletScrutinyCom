---
wsId: cobru
title: Cobru
altTitle: 
authors:
- danny
appId: com.cobru.cobru
appCountry: us
idd: '1574045983'
released: '2021-06-29T07:00:00Z'
updated: 2023-03-09
version: 4.0.91
stars: 5
reviews: 1
size: '51394560'
website: https://cobru.co
repository: 
issue: 
icon: com.cobru.cobru.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-03-30
signer: 
reviewArchive: 
twitter: cobruapp
social:
- https://www.facebook.com/cobruapp
- https://www.instagram.com/cobruapp/
features: 

---

{% include copyFromAndroid.html %}
