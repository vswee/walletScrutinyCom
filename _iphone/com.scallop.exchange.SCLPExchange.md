---
wsId: scallopExchange
title: Scallop Exchange
altTitle: 
authors:
- danny
appId: com.scallop.exchange.SCLPExchange
appCountry: us
idd: '1628290736'
released: '2022-06-15T07:00:00Z'
updated: 2022-08-16
version: 1.0.2
stars: 4.7
reviews: 10
size: '174145536'
website: http://www.scallop.exchange
repository: 
issue: 
icon: com.scallop.exchange.SCLPExchange.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-03-30
signer: 
reviewArchive: 
twitter: ScallopOfficial
social:
- https://www.linkedin.com/company/scallopx/
features: 

---

{% include copyFromAndroid.html %}
