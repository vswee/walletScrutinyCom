---
wsId: deribitOptions
title: 'Deribit: BTC Options & Futures'
altTitle: 
authors:
- danny
appId: com.deribit
appCountry: gb
idd: '1293674041'
released: '2017-11-17T06:33:50Z'
updated: 2023-03-26
version: 3.1.0
stars: 5
reviews: 1
size: '27543552'
website: http://www.deribit.com
repository: 
issue: 
icon: com.deribit.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-03-31
signer: 
reviewArchive: 
twitter: deribitexchange
social:
- https://www.linkedin.com/company/deribit/
- https://www.reddit.com/r/DeribitExchange/
- https://t.me/deribit
- https://www.youtube.com/channel/UCbHKjlFogkOD0lUVeb5CsGA
features: 

---

{% include copyFromAndroid.html %}