---
wsId: bitAfrika
title: BitAfrika - Buy & Sell Crypto
altTitle: 
authors:
- danny
appId: app.bitafrika.com
appCountry: us
idd: '1577083741'
released: '2021-07-28T07:00:00Z'
updated: 2023-02-21
version: 4.2.0
stars: 4.7
reviews: 625
size: '25928704'
website: https://bitafrika.com/contact
repository: 
issue: 
icon: app.bitafrika.com.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-04-05
signer: 
reviewArchive: 
twitter: _bitafrika
social:
- https://www.facebook.com/bitafrika
- https://www.instagram.com/bitafrika/
features: 

---

{% include copyFromAndroid.html %}


