---
wsId: coinZoom
title: 'CoinZoom: Buy Bitcoin & Crypto'
altTitle: 
authors:
- danny
appId: com.ios.coinzoomsimple
appCountry: us
idd: '1575983875'
released: '2022-01-21T08:00:00Z'
updated: 2023-04-03
version: 1.0.38
stars: 4.5
reviews: 378
size: '101388288'
website: http://www.coinzoom.com
repository: 
issue: 
icon: com.ios.coinzoomsimple.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-03-30
signer: 
reviewArchive: 
twitter: GetCoinZoom
social:
- https://www.facebook.com/CoinZoom
- https://www.linkedin.com/company/coinzoomhq/
features: 

---

{% include copyFromAndroid.html %}
