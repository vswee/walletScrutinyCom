---
wsId: conglex
title: Conglex
altTitle: 
authors:
- danny
appId: com.conglex.mobile
appCountry: us
idd: '1626252670'
released: '2022-06-03T07:00:00Z'
updated: 2023-03-08
version: 1.3.9
stars: 0
reviews: 0
size: '47794176'
website: https://conglex.com/
repository: 
issue: 
icon: com.conglex.mobile.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-03-30
signer: 
reviewArchive: 
twitter: conglexglobal
social:
- https://www.linkedin.com/company/conglex-nigeria/
features: 

---

{% include copyFromAndroid.html %}

