---
wsId: vsolidusnova
title: Nova-Wallet
altTitle: 
authors:
- danny
appId: net.novawallet.ios
appCountry: gb
idd: '1592044359'
released: '2021-10-27T07:00:00Z'
updated: 2022-07-29
version: 2.2.3
stars: 4.9
reviews: 81
size: '34328576'
website: 
repository: 
issue: 
icon: net.novawallet.ios.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-03-05
signer: 
reviewArchive: 
twitter: solidus_v
social:
- https://www.facebook.com/vsolidus
- https://www.linkedin.com/company/vsolidus/
- https://www.youtube.com/channel/UCEf9OP4qRMwmvJuLx1p6CCw
- https://www.instagram.com/vsolidus/
features: 

---

{% include copyFromAndroid.html %}

